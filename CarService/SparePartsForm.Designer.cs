﻿namespace CarService
{
    partial class SparePartsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNew = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.lblSpPartNumber = new System.Windows.Forms.Label();
            this.lblSpPartCategory = new System.Windows.Forms.Label();
            this.lblSpPartName = new System.Windows.Forms.Label();
            this.cbSpPartNumber = new System.Windows.Forms.ComboBox();
            this.cbSpPartCategory = new System.Windows.Forms.ComboBox();
            this.cbSpPartName = new System.Windows.Forms.ComboBox();
            this.lblSpPartDescription = new System.Windows.Forms.Label();
            this.txtBoxSpPartDescr = new System.Windows.Forms.TextBox();
            this.lblSpPartPrice = new System.Windows.Forms.Label();
            this.txtSpPartPrice = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(195, 352);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(100, 23);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "Нов запис";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(301, 352);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(100, 23);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Редактиране";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(407, 352);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(100, 23);
            this.btnDel.TabIndex = 2;
            this.btnDel.Text = "Изтриване";
            this.btnDel.UseVisualStyleBackColor = true;
            // 
            // lblSpPartNumber
            // 
            this.lblSpPartNumber.AutoSize = true;
            this.lblSpPartNumber.Location = new System.Drawing.Point(12, 20);
            this.lblSpPartNumber.Name = "lblSpPartNumber";
            this.lblSpPartNumber.Size = new System.Drawing.Size(104, 17);
            this.lblSpPartNumber.TabIndex = 3;
            this.lblSpPartNumber.Text = "Каталожен №*";
            // 
            // lblSpPartCategory
            // 
            this.lblSpPartCategory.AutoSize = true;
            this.lblSpPartCategory.Location = new System.Drawing.Point(178, 20);
            this.lblSpPartCategory.Name = "lblSpPartCategory";
            this.lblSpPartCategory.Size = new System.Drawing.Size(77, 17);
            this.lblSpPartCategory.TabIndex = 4;
            this.lblSpPartCategory.Text = "Категория";
            // 
            // lblSpPartName
            // 
            this.lblSpPartName.AutoSize = true;
            this.lblSpPartName.Location = new System.Drawing.Point(344, 20);
            this.lblSpPartName.Name = "lblSpPartName";
            this.lblSpPartName.Size = new System.Drawing.Size(40, 17);
            this.lblSpPartName.TabIndex = 5;
            this.lblSpPartName.Text = "Име*";
            // 
            // cbSpPartNumber
            // 
            this.cbSpPartNumber.FormattingEnabled = true;
            this.cbSpPartNumber.Location = new System.Drawing.Point(15, 40);
            this.cbSpPartNumber.Name = "cbSpPartNumber";
            this.cbSpPartNumber.Size = new System.Drawing.Size(160, 24);
            this.cbSpPartNumber.TabIndex = 6;
            this.cbSpPartNumber.TextChanged += new System.EventHandler(this.cbSpPartNumber_TextChanged);
            // 
            // cbSpPartCategory
            // 
            this.cbSpPartCategory.FormattingEnabled = true;
            this.cbSpPartCategory.Location = new System.Drawing.Point(181, 40);
            this.cbSpPartCategory.Name = "cbSpPartCategory";
            this.cbSpPartCategory.Size = new System.Drawing.Size(160, 24);
            this.cbSpPartCategory.TabIndex = 7;
            // 
            // cbSpPartName
            // 
            this.cbSpPartName.FormattingEnabled = true;
            this.cbSpPartName.Location = new System.Drawing.Point(347, 40);
            this.cbSpPartName.Name = "cbSpPartName";
            this.cbSpPartName.Size = new System.Drawing.Size(160, 24);
            this.cbSpPartName.TabIndex = 8;
            // 
            // lblSpPartDescription
            // 
            this.lblSpPartDescription.AutoSize = true;
            this.lblSpPartDescription.Location = new System.Drawing.Point(12, 79);
            this.lblSpPartDescription.Name = "lblSpPartDescription";
            this.lblSpPartDescription.Size = new System.Drawing.Size(74, 17);
            this.lblSpPartDescription.TabIndex = 9;
            this.lblSpPartDescription.Text = "Описание";
            // 
            // txtBoxSpPartDescr
            // 
            this.txtBoxSpPartDescr.Location = new System.Drawing.Point(15, 99);
            this.txtBoxSpPartDescr.Multiline = true;
            this.txtBoxSpPartDescr.Name = "txtBoxSpPartDescr";
            this.txtBoxSpPartDescr.Size = new System.Drawing.Size(492, 165);
            this.txtBoxSpPartDescr.TabIndex = 10;
            // 
            // lblSpPartPrice
            // 
            this.lblSpPartPrice.AutoSize = true;
            this.lblSpPartPrice.Location = new System.Drawing.Point(298, 297);
            this.lblSpPartPrice.Name = "lblSpPartPrice";
            this.lblSpPartPrice.Size = new System.Drawing.Size(48, 17);
            this.lblSpPartPrice.TabIndex = 11;
            this.lblSpPartPrice.Text = "Цена*";
            // 
            // txtSpPartPrice
            // 
            this.txtSpPartPrice.Location = new System.Drawing.Point(347, 294);
            this.txtSpPartPrice.Name = "txtSpPartPrice";
            this.txtSpPartPrice.Size = new System.Drawing.Size(160, 22);
            this.txtSpPartPrice.TabIndex = 12;
            // 
            // SparePartsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 387);
            this.Controls.Add(this.txtSpPartPrice);
            this.Controls.Add(this.lblSpPartPrice);
            this.Controls.Add(this.txtBoxSpPartDescr);
            this.Controls.Add(this.lblSpPartDescription);
            this.Controls.Add(this.cbSpPartName);
            this.Controls.Add(this.cbSpPartCategory);
            this.Controls.Add(this.cbSpPartNumber);
            this.Controls.Add(this.lblSpPartName);
            this.Controls.Add(this.lblSpPartCategory);
            this.Controls.Add(this.lblSpPartNumber);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnNew);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "SparePartsForm";
            this.Text = "Резервни части";
            this.Load += new System.EventHandler(this.SparePartsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Label lblSpPartNumber;
        private System.Windows.Forms.Label lblSpPartCategory;
        private System.Windows.Forms.Label lblSpPartName;
        private System.Windows.Forms.ComboBox cbSpPartNumber;
        private System.Windows.Forms.ComboBox cbSpPartCategory;
        private System.Windows.Forms.ComboBox cbSpPartName;
        private System.Windows.Forms.Label lblSpPartDescription;
        private System.Windows.Forms.TextBox txtBoxSpPartDescr;
        private System.Windows.Forms.Label lblSpPartPrice;
        private System.Windows.Forms.TextBox txtSpPartPrice;
    }
}