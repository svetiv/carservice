﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace CarService
{
    class DatabaseInteraction
    {
        OracleConnection conn;
        string oradb = "Data Source=XE;User Id=cars;Password=cars;";
        DataTable dt;

        public DataTable ReadData(string table, string command)
        {
            conn = new OracleConnection(oradb);
            try
            {
                conn.Open();
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = conn;
                cmd.CommandText = command;
                cmd.CommandType = CommandType.Text;
                OracleDataReader dr = cmd.ExecuteReader();
                dt = new DataTable();
                dt.Load(dr);
            }
            finally
            {
                conn.Dispose();
            }
            return dt;
        }

        public int WriteData(string command)
        {
            int rowUpdated = 0;
            conn = new OracleConnection(oradb);
            try
            {
                conn.Open();
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = conn;
                cmd.CommandText = command;
                cmd.CommandType = CommandType.Text;
                rowUpdated = cmd.ExecuteNonQuery();
                return rowUpdated;
            }
            finally
            {
                conn.Dispose();
            }
        }
    }
}
