﻿namespace CarService
{
    partial class CarsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCarsRegNumber = new System.Windows.Forms.Label();
            this.lblCarsBrand = new System.Windows.Forms.Label();
            this.lblCarsModel = new System.Windows.Forms.Label();
            this.lblCarsProdYear = new System.Windows.Forms.Label();
            this.lblCarsEngineNumber = new System.Windows.Forms.Label();
            this.lblCarsVIN = new System.Windows.Forms.Label();
            this.lblCarsColor = new System.Windows.Forms.Label();
            this.lblCarsEnginCapac = new System.Windows.Forms.Label();
            this.lblCarsDescr = new System.Windows.Forms.Label();
            this.lblCarsOwnerTelph = new System.Windows.Forms.Label();
            this.lblCarsOwnerName = new System.Windows.Forms.Label();
            this.txtCarsSearchNumber = new System.Windows.Forms.TextBox();
            this.btnCarsNew = new System.Windows.Forms.Button();
            this.btnCarsEdit = new System.Windows.Forms.Button();
            this.btnCarsDelete = new System.Windows.Forms.Button();
            this.txtCarsRegNumber = new System.Windows.Forms.TextBox();
            this.txtCarsBrand = new System.Windows.Forms.TextBox();
            this.txtCarsModel = new System.Windows.Forms.TextBox();
            this.txtCarsProdYear = new System.Windows.Forms.TextBox();
            this.txtCarsEngineNumber = new System.Windows.Forms.TextBox();
            this.txtCarsVIN = new System.Windows.Forms.TextBox();
            this.txtCarsColor = new System.Windows.Forms.TextBox();
            this.txtCarsEnginCapac = new System.Windows.Forms.TextBox();
            this.txtCarsDescr = new System.Windows.Forms.TextBox();
            this.txtCarsOwnerName = new System.Windows.Forms.TextBox();
            this.txtCarsEGN = new System.Windows.Forms.TextBox();
            this.txtCarsOwnerTelph = new System.Windows.Forms.TextBox();
            this.lblCarsSearch = new System.Windows.Forms.Label();
            this.lblCarsEGN = new System.Windows.Forms.Label();
            this.btnCarsSearch = new System.Windows.Forms.Button();
            this.grBoxCarsSearch = new System.Windows.Forms.GroupBox();
            this.grBoxCarsOwner = new System.Windows.Forms.GroupBox();
            this.txtCarsRecordNumber = new System.Windows.Forms.TextBox();
            this.lblCarsRecordNumber = new System.Windows.Forms.Label();
            this.grBoxCarsCar = new System.Windows.Forms.GroupBox();
            this.lblCarsNote = new System.Windows.Forms.Label();
            this.grBoxCarsSearch.SuspendLayout();
            this.grBoxCarsOwner.SuspendLayout();
            this.grBoxCarsCar.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCarsRegNumber
            // 
            this.lblCarsRegNumber.AutoSize = true;
            this.lblCarsRegNumber.Location = new System.Drawing.Point(7, 30);
            this.lblCarsRegNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsRegNumber.Name = "lblCarsRegNumber";
            this.lblCarsRegNumber.Size = new System.Drawing.Size(166, 17);
            this.lblCarsRegNumber.TabIndex = 21;
            this.lblCarsRegNumber.Text = "Регистрационен номер*";
            // 
            // lblCarsBrand
            // 
            this.lblCarsBrand.AutoSize = true;
            this.lblCarsBrand.Location = new System.Drawing.Point(7, 87);
            this.lblCarsBrand.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsBrand.Name = "lblCarsBrand";
            this.lblCarsBrand.Size = new System.Drawing.Size(55, 17);
            this.lblCarsBrand.TabIndex = 24;
            this.lblCarsBrand.Text = "Марка*";
            // 
            // lblCarsModel
            // 
            this.lblCarsModel.AutoSize = true;
            this.lblCarsModel.Location = new System.Drawing.Point(211, 87);
            this.lblCarsModel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsModel.Name = "lblCarsModel";
            this.lblCarsModel.Size = new System.Drawing.Size(56, 17);
            this.lblCarsModel.TabIndex = 25;
            this.lblCarsModel.Text = "Модел*";
            // 
            // lblCarsProdYear
            // 
            this.lblCarsProdYear.AutoSize = true;
            this.lblCarsProdYear.Location = new System.Drawing.Point(418, 87);
            this.lblCarsProdYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsProdYear.Name = "lblCarsProdYear";
            this.lblCarsProdYear.Size = new System.Drawing.Size(171, 17);
            this.lblCarsProdYear.TabIndex = 26;
            this.lblCarsProdYear.Text = "Година на производство";
            // 
            // lblCarsEngineNumber
            // 
            this.lblCarsEngineNumber.AutoSize = true;
            this.lblCarsEngineNumber.Location = new System.Drawing.Point(211, 30);
            this.lblCarsEngineNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsEngineNumber.Name = "lblCarsEngineNumber";
            this.lblCarsEngineNumber.Size = new System.Drawing.Size(139, 17);
            this.lblCarsEngineNumber.TabIndex = 22;
            this.lblCarsEngineNumber.Text = "Номер на двигател*";
            // 
            // lblCarsVIN
            // 
            this.lblCarsVIN.AutoSize = true;
            this.lblCarsVIN.Location = new System.Drawing.Point(418, 30);
            this.lblCarsVIN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsVIN.Name = "lblCarsVIN";
            this.lblCarsVIN.Size = new System.Drawing.Size(113, 17);
            this.lblCarsVIN.TabIndex = 23;
            this.lblCarsVIN.Text = "Номер на рама*";
            // 
            // lblCarsColor
            // 
            this.lblCarsColor.AutoSize = true;
            this.lblCarsColor.Location = new System.Drawing.Point(211, 148);
            this.lblCarsColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsColor.Name = "lblCarsColor";
            this.lblCarsColor.Size = new System.Drawing.Size(41, 17);
            this.lblCarsColor.TabIndex = 28;
            this.lblCarsColor.Text = "Цвят";
            // 
            // lblCarsEnginCapac
            // 
            this.lblCarsEnginCapac.AutoSize = true;
            this.lblCarsEnginCapac.Location = new System.Drawing.Point(7, 148);
            this.lblCarsEnginCapac.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsEnginCapac.Name = "lblCarsEnginCapac";
            this.lblCarsEnginCapac.Size = new System.Drawing.Size(192, 17);
            this.lblCarsEnginCapac.TabIndex = 27;
            this.lblCarsEnginCapac.Text = "Работен обем на двигателя";
            // 
            // lblCarsDescr
            // 
            this.lblCarsDescr.AutoSize = true;
            this.lblCarsDescr.Location = new System.Drawing.Point(7, 211);
            this.lblCarsDescr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsDescr.Name = "lblCarsDescr";
            this.lblCarsDescr.Size = new System.Drawing.Size(74, 17);
            this.lblCarsDescr.TabIndex = 29;
            this.lblCarsDescr.Text = "Описание";
            // 
            // lblCarsOwnerTelph
            // 
            this.lblCarsOwnerTelph.AutoSize = true;
            this.lblCarsOwnerTelph.Location = new System.Drawing.Point(474, 38);
            this.lblCarsOwnerTelph.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsOwnerTelph.Name = "lblCarsOwnerTelph";
            this.lblCarsOwnerTelph.Size = new System.Drawing.Size(73, 17);
            this.lblCarsOwnerTelph.TabIndex = 32;
            this.lblCarsOwnerTelph.Text = "Телефон*";
            // 
            // lblCarsOwnerName
            // 
            this.lblCarsOwnerName.AutoSize = true;
            this.lblCarsOwnerName.Location = new System.Drawing.Point(4, 38);
            this.lblCarsOwnerName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsOwnerName.Name = "lblCarsOwnerName";
            this.lblCarsOwnerName.Size = new System.Drawing.Size(40, 17);
            this.lblCarsOwnerName.TabIndex = 30;
            this.lblCarsOwnerName.Text = "Име*";
            // 
            // txtCarsSearchNumber
            // 
            this.txtCarsSearchNumber.Location = new System.Drawing.Point(33, 33);
            this.txtCarsSearchNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsSearchNumber.Name = "txtCarsSearchNumber";
            this.txtCarsSearchNumber.Size = new System.Drawing.Size(156, 22);
            this.txtCarsSearchNumber.TabIndex = 18;
            // 
            // btnCarsNew
            // 
            this.btnCarsNew.Location = new System.Drawing.Point(355, 666);
            this.btnCarsNew.Margin = new System.Windows.Forms.Padding(4);
            this.btnCarsNew.Name = "btnCarsNew";
            this.btnCarsNew.Size = new System.Drawing.Size(100, 28);
            this.btnCarsNew.TabIndex = 15;
            this.btnCarsNew.Text = "Нов запис";
            this.btnCarsNew.UseVisualStyleBackColor = true;
            this.btnCarsNew.Click += new System.EventHandler(this.btnCarsNew_Click);
            // 
            // btnCarsEdit
            // 
            this.btnCarsEdit.Location = new System.Drawing.Point(463, 666);
            this.btnCarsEdit.Margin = new System.Windows.Forms.Padding(4);
            this.btnCarsEdit.Name = "btnCarsEdit";
            this.btnCarsEdit.Size = new System.Drawing.Size(117, 28);
            this.btnCarsEdit.TabIndex = 16;
            this.btnCarsEdit.Text = "Редактиране";
            this.btnCarsEdit.UseVisualStyleBackColor = true;
            this.btnCarsEdit.Click += new System.EventHandler(this.btnCarsEdit_Click);
            // 
            // btnCarsDelete
            // 
            this.btnCarsDelete.Location = new System.Drawing.Point(588, 666);
            this.btnCarsDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnCarsDelete.Name = "btnCarsDelete";
            this.btnCarsDelete.Size = new System.Drawing.Size(100, 28);
            this.btnCarsDelete.TabIndex = 17;
            this.btnCarsDelete.Text = "Изтриване";
            this.btnCarsDelete.UseVisualStyleBackColor = true;
            this.btnCarsDelete.Click += new System.EventHandler(this.btnCarsDelete_Click);
            // 
            // txtCarsRegNumber
            // 
            this.txtCarsRegNumber.Location = new System.Drawing.Point(11, 51);
            this.txtCarsRegNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsRegNumber.Name = "txtCarsRegNumber";
            this.txtCarsRegNumber.Size = new System.Drawing.Size(200, 22);
            this.txtCarsRegNumber.TabIndex = 2;
            // 
            // txtCarsBrand
            // 
            this.txtCarsBrand.Location = new System.Drawing.Point(10, 108);
            this.txtCarsBrand.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsBrand.Name = "txtCarsBrand";
            this.txtCarsBrand.Size = new System.Drawing.Size(200, 22);
            this.txtCarsBrand.TabIndex = 5;
            // 
            // txtCarsModel
            // 
            this.txtCarsModel.Location = new System.Drawing.Point(214, 108);
            this.txtCarsModel.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsModel.Name = "txtCarsModel";
            this.txtCarsModel.Size = new System.Drawing.Size(200, 22);
            this.txtCarsModel.TabIndex = 6;
            // 
            // txtCarsProdYear
            // 
            this.txtCarsProdYear.Location = new System.Drawing.Point(421, 108);
            this.txtCarsProdYear.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsProdYear.Name = "txtCarsProdYear";
            this.txtCarsProdYear.Size = new System.Drawing.Size(200, 22);
            this.txtCarsProdYear.TabIndex = 7;
            // 
            // txtCarsEngineNumber
            // 
            this.txtCarsEngineNumber.Location = new System.Drawing.Point(214, 51);
            this.txtCarsEngineNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsEngineNumber.Name = "txtCarsEngineNumber";
            this.txtCarsEngineNumber.Size = new System.Drawing.Size(200, 22);
            this.txtCarsEngineNumber.TabIndex = 3;
            // 
            // txtCarsVIN
            // 
            this.txtCarsVIN.Location = new System.Drawing.Point(421, 51);
            this.txtCarsVIN.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsVIN.Name = "txtCarsVIN";
            this.txtCarsVIN.Size = new System.Drawing.Size(200, 22);
            this.txtCarsVIN.TabIndex = 4;
            // 
            // txtCarsColor
            // 
            this.txtCarsColor.Location = new System.Drawing.Point(214, 169);
            this.txtCarsColor.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsColor.Name = "txtCarsColor";
            this.txtCarsColor.Size = new System.Drawing.Size(199, 22);
            this.txtCarsColor.TabIndex = 9;
            // 
            // txtCarsEnginCapac
            // 
            this.txtCarsEnginCapac.Location = new System.Drawing.Point(10, 169);
            this.txtCarsEnginCapac.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsEnginCapac.Name = "txtCarsEnginCapac";
            this.txtCarsEnginCapac.Size = new System.Drawing.Size(199, 22);
            this.txtCarsEnginCapac.TabIndex = 8;
            // 
            // txtCarsDescr
            // 
            this.txtCarsDescr.Location = new System.Drawing.Point(10, 232);
            this.txtCarsDescr.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsDescr.Multiline = true;
            this.txtCarsDescr.Name = "txtCarsDescr";
            this.txtCarsDescr.Size = new System.Drawing.Size(651, 98);
            this.txtCarsDescr.TabIndex = 10;
            // 
            // txtCarsOwnerName
            // 
            this.txtCarsOwnerName.Location = new System.Drawing.Point(7, 59);
            this.txtCarsOwnerName.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsOwnerName.Name = "txtCarsOwnerName";
            this.txtCarsOwnerName.Size = new System.Drawing.Size(260, 22);
            this.txtCarsOwnerName.TabIndex = 12;
            // 
            // txtCarsEGN
            // 
            this.txtCarsEGN.Location = new System.Drawing.Point(277, 59);
            this.txtCarsEGN.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsEGN.Name = "txtCarsEGN";
            this.txtCarsEGN.Size = new System.Drawing.Size(192, 22);
            this.txtCarsEGN.TabIndex = 13;
            // 
            // txtCarsOwnerTelph
            // 
            this.txtCarsOwnerTelph.Location = new System.Drawing.Point(477, 59);
            this.txtCarsOwnerTelph.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsOwnerTelph.Name = "txtCarsOwnerTelph";
            this.txtCarsOwnerTelph.Size = new System.Drawing.Size(187, 22);
            this.txtCarsOwnerTelph.TabIndex = 14;
            // 
            // lblCarsSearch
            // 
            this.lblCarsSearch.AutoSize = true;
            this.lblCarsSearch.Location = new System.Drawing.Point(7, 36);
            this.lblCarsSearch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsSearch.Name = "lblCarsSearch";
            this.lblCarsSearch.Size = new System.Drawing.Size(22, 17);
            this.lblCarsSearch.TabIndex = 34;
            this.lblCarsSearch.Text = "№";
            // 
            // lblCarsEGN
            // 
            this.lblCarsEGN.AutoSize = true;
            this.lblCarsEGN.Location = new System.Drawing.Point(274, 38);
            this.lblCarsEGN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsEGN.Name = "lblCarsEGN";
            this.lblCarsEGN.Size = new System.Drawing.Size(40, 17);
            this.lblCarsEGN.TabIndex = 31;
            this.lblCarsEGN.Text = "ЕГН*";
            // 
            // btnCarsSearch
            // 
            this.btnCarsSearch.Location = new System.Drawing.Point(199, 30);
            this.btnCarsSearch.Margin = new System.Windows.Forms.Padding(4);
            this.btnCarsSearch.Name = "btnCarsSearch";
            this.btnCarsSearch.Size = new System.Drawing.Size(68, 28);
            this.btnCarsSearch.TabIndex = 19;
            this.btnCarsSearch.Text = "Намери";
            this.btnCarsSearch.UseVisualStyleBackColor = true;
            this.btnCarsSearch.Click += new System.EventHandler(this.btnCarsSearch_Click);
            // 
            // grBoxCarsSearch
            // 
            this.grBoxCarsSearch.Controls.Add(this.lblCarsSearch);
            this.grBoxCarsSearch.Controls.Add(this.btnCarsSearch);
            this.grBoxCarsSearch.Controls.Add(this.txtCarsSearchNumber);
            this.grBoxCarsSearch.Location = new System.Drawing.Point(13, 539);
            this.grBoxCarsSearch.Name = "grBoxCarsSearch";
            this.grBoxCarsSearch.Size = new System.Drawing.Size(274, 76);
            this.grBoxCarsSearch.TabIndex = 33;
            this.grBoxCarsSearch.TabStop = false;
            this.grBoxCarsSearch.Text = "Търсене по Регистрационен Номер";
            // 
            // grBoxCarsOwner
            // 
            this.grBoxCarsOwner.Controls.Add(this.txtCarsOwnerName);
            this.grBoxCarsOwner.Controls.Add(this.txtCarsEGN);
            this.grBoxCarsOwner.Controls.Add(this.lblCarsEGN);
            this.grBoxCarsOwner.Controls.Add(this.txtCarsOwnerTelph);
            this.grBoxCarsOwner.Controls.Add(this.lblCarsOwnerName);
            this.grBoxCarsOwner.Controls.Add(this.lblCarsOwnerTelph);
            this.grBoxCarsOwner.Location = new System.Drawing.Point(13, 373);
            this.grBoxCarsOwner.Name = "grBoxCarsOwner";
            this.grBoxCarsOwner.Size = new System.Drawing.Size(675, 100);
            this.grBoxCarsOwner.TabIndex = 11;
            this.grBoxCarsOwner.TabStop = false;
            this.grBoxCarsOwner.Text = "Данни за Собственик";
            // 
            // txtCarsRecordNumber
            // 
            this.txtCarsRecordNumber.Location = new System.Drawing.Point(13, 669);
            this.txtCarsRecordNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarsRecordNumber.Name = "txtCarsRecordNumber";
            this.txtCarsRecordNumber.Size = new System.Drawing.Size(274, 22);
            this.txtCarsRecordNumber.TabIndex = 20;
            // 
            // lblCarsRecordNumber
            // 
            this.lblCarsRecordNumber.AutoSize = true;
            this.lblCarsRecordNumber.Location = new System.Drawing.Point(10, 648);
            this.lblCarsRecordNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsRecordNumber.Name = "lblCarsRecordNumber";
            this.lblCarsRecordNumber.Size = new System.Drawing.Size(113, 17);
            this.lblCarsRecordNumber.TabIndex = 35;
            this.lblCarsRecordNumber.Text = "Номер на запис";
            // 
            // grBoxCarsCar
            // 
            this.grBoxCarsCar.Controls.Add(this.txtCarsRegNumber);
            this.grBoxCarsCar.Controls.Add(this.txtCarsDescr);
            this.grBoxCarsCar.Controls.Add(this.lblCarsRegNumber);
            this.grBoxCarsCar.Controls.Add(this.txtCarsColor);
            this.grBoxCarsCar.Controls.Add(this.lblCarsDescr);
            this.grBoxCarsCar.Controls.Add(this.txtCarsEnginCapac);
            this.grBoxCarsCar.Controls.Add(this.txtCarsEngineNumber);
            this.grBoxCarsCar.Controls.Add(this.lblCarsEngineNumber);
            this.grBoxCarsCar.Controls.Add(this.lblCarsColor);
            this.grBoxCarsCar.Controls.Add(this.txtCarsProdYear);
            this.grBoxCarsCar.Controls.Add(this.txtCarsVIN);
            this.grBoxCarsCar.Controls.Add(this.lblCarsEnginCapac);
            this.grBoxCarsCar.Controls.Add(this.txtCarsModel);
            this.grBoxCarsCar.Controls.Add(this.lblCarsVIN);
            this.grBoxCarsCar.Controls.Add(this.txtCarsBrand);
            this.grBoxCarsCar.Controls.Add(this.lblCarsBrand);
            this.grBoxCarsCar.Controls.Add(this.lblCarsProdYear);
            this.grBoxCarsCar.Controls.Add(this.lblCarsModel);
            this.grBoxCarsCar.Location = new System.Drawing.Point(13, 12);
            this.grBoxCarsCar.Name = "grBoxCarsCar";
            this.grBoxCarsCar.Size = new System.Drawing.Size(675, 344);
            this.grBoxCarsCar.TabIndex = 1;
            this.grBoxCarsCar.TabStop = false;
            this.grBoxCarsCar.Text = "Данни за Автомобил";
            // 
            // lblCarsNote
            // 
            this.lblCarsNote.AutoSize = true;
            this.lblCarsNote.BackColor = System.Drawing.Color.Red;
            this.lblCarsNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCarsNote.Location = new System.Drawing.Point(10, 718);
            this.lblCarsNote.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarsNote.Name = "lblCarsNote";
            this.lblCarsNote.Size = new System.Drawing.Size(586, 20);
            this.lblCarsNote.TabIndex = 36;
            this.lblCarsNote.Text = "Забележка: Полетата със звезда са задължителни за въвеждане!!!";
            // 
            // CarsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 752);
            this.Controls.Add(this.lblCarsNote);
            this.Controls.Add(this.txtCarsRecordNumber);
            this.Controls.Add(this.grBoxCarsCar);
            this.Controls.Add(this.lblCarsRecordNumber);
            this.Controls.Add(this.grBoxCarsOwner);
            this.Controls.Add(this.grBoxCarsSearch);
            this.Controls.Add(this.btnCarsDelete);
            this.Controls.Add(this.btnCarsEdit);
            this.Controls.Add(this.btnCarsNew);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "CarsForm";
            this.Text = "Автомобили";
            this.grBoxCarsSearch.ResumeLayout(false);
            this.grBoxCarsSearch.PerformLayout();
            this.grBoxCarsOwner.ResumeLayout(false);
            this.grBoxCarsOwner.PerformLayout();
            this.grBoxCarsCar.ResumeLayout(false);
            this.grBoxCarsCar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCarsRegNumber;
        private System.Windows.Forms.Label lblCarsBrand;
        private System.Windows.Forms.Label lblCarsModel;
        private System.Windows.Forms.Label lblCarsProdYear;
        private System.Windows.Forms.Label lblCarsEngineNumber;
        private System.Windows.Forms.Label lblCarsVIN;
        private System.Windows.Forms.Label lblCarsColor;
        private System.Windows.Forms.Label lblCarsEnginCapac;
        private System.Windows.Forms.Label lblCarsDescr;
        private System.Windows.Forms.Label lblCarsOwnerTelph;
        private System.Windows.Forms.Label lblCarsOwnerName;
        private System.Windows.Forms.Label lblCarsEGN;
        private System.Windows.Forms.TextBox txtCarsSearchNumber;
        private System.Windows.Forms.Button btnCarsNew;
        private System.Windows.Forms.Button btnCarsEdit;
        private System.Windows.Forms.Button btnCarsDelete;
        private System.Windows.Forms.TextBox txtCarsRegNumber;
        private System.Windows.Forms.TextBox txtCarsBrand;
        private System.Windows.Forms.TextBox txtCarsModel;
        private System.Windows.Forms.TextBox txtCarsProdYear;
        private System.Windows.Forms.TextBox txtCarsEngineNumber;
        private System.Windows.Forms.TextBox txtCarsVIN;
        private System.Windows.Forms.TextBox txtCarsColor;
        private System.Windows.Forms.TextBox txtCarsEnginCapac;
        private System.Windows.Forms.TextBox txtCarsDescr;
        private System.Windows.Forms.TextBox txtCarsOwnerName;
        private System.Windows.Forms.TextBox txtCarsEGN;
        private System.Windows.Forms.TextBox txtCarsOwnerTelph;
        private System.Windows.Forms.Label lblCarsSearch;
        private System.Windows.Forms.Button btnCarsSearch;
        private System.Windows.Forms.GroupBox grBoxCarsSearch;
        private System.Windows.Forms.GroupBox grBoxCarsOwner;
        private System.Windows.Forms.TextBox txtCarsRecordNumber;
        private System.Windows.Forms.Label lblCarsRecordNumber;
        private System.Windows.Forms.GroupBox grBoxCarsCar;
        private System.Windows.Forms.Label lblCarsNote;
    }
}