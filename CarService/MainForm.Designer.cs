﻿namespace CarService
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.dataBaseMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sparePartsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repairCardsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviseCardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeesReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainDataGridView = new System.Windows.Forms.DataGridView();
            this.mainGrBox = new System.Windows.Forms.GroupBox();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGridView)).BeginInit();
            this.mainGrBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataBaseMenuItem,
            this.reportsMenuItem,
            this.settingsMenuItem,
            this.helpMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.mainMenu.Size = new System.Drawing.Size(1067, 28);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // dataBaseMenuItem
            // 
            this.dataBaseMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.carsMenuItem,
            this.sparePartsMenuItem,
            this.repairCardsMenuItem,
            this.employeesToolStripMenuItem});
            this.dataBaseMenuItem.Name = "dataBaseMenuItem";
            this.dataBaseMenuItem.Size = new System.Drawing.Size(102, 24);
            this.dataBaseMenuItem.Text = "База Данни";
            // 
            // carsMenuItem
            // 
            this.carsMenuItem.Name = "carsMenuItem";
            this.carsMenuItem.Size = new System.Drawing.Size(196, 26);
            this.carsMenuItem.Text = "Автомобили";
            this.carsMenuItem.Click += new System.EventHandler(this.carsMenuItem_Click);
            // 
            // sparePartsMenuItem
            // 
            this.sparePartsMenuItem.Name = "sparePartsMenuItem";
            this.sparePartsMenuItem.Size = new System.Drawing.Size(196, 26);
            this.sparePartsMenuItem.Text = "Резервни части";
            this.sparePartsMenuItem.Click += new System.EventHandler(this.sparePartsMenuItem_Click);
            // 
            // repairCardsMenuItem
            // 
            this.repairCardsMenuItem.Name = "repairCardsMenuItem";
            this.repairCardsMenuItem.Size = new System.Drawing.Size(196, 26);
            this.repairCardsMenuItem.Text = "Ремонтни карти";
            this.repairCardsMenuItem.Click += new System.EventHandler(this.repairCardsMenuItem_Click);
            // 
            // employeesToolStripMenuItem
            // 
            this.employeesToolStripMenuItem.Name = "employeesToolStripMenuItem";
            this.employeesToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.employeesToolStripMenuItem.Text = "Служители";
            this.employeesToolStripMenuItem.Click += new System.EventHandler(this.employeesToolStripMenuItem_Click);
            // 
            // reportsMenuItem
            // 
            this.reportsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serviseCardsToolStripMenuItem,
            this.carsToolStripMenuItem,
            this.clientsToolStripMenuItem,
            this.employeesReportToolStripMenuItem});
            this.reportsMenuItem.Name = "reportsMenuItem";
            this.reportsMenuItem.Size = new System.Drawing.Size(80, 24);
            this.reportsMenuItem.Text = "Справки";
            // 
            // serviseCardsToolStripMenuItem
            // 
            this.serviseCardsToolStripMenuItem.Name = "serviseCardsToolStripMenuItem";
            this.serviseCardsToolStripMenuItem.Size = new System.Drawing.Size(198, 26);
            this.serviseCardsToolStripMenuItem.Text = "Ремонтни Карти";
            this.serviseCardsToolStripMenuItem.Click += new System.EventHandler(this.serviceCardsToolStripMenuItem_Click);
            // 
            // carsToolStripMenuItem
            // 
            this.carsToolStripMenuItem.Name = "carsToolStripMenuItem";
            this.carsToolStripMenuItem.Size = new System.Drawing.Size(198, 26);
            this.carsToolStripMenuItem.Text = "Автомобили";
            this.carsToolStripMenuItem.Click += new System.EventHandler(this.carsToolStripMenuItem_Click);
            // 
            // clientsToolStripMenuItem
            // 
            this.clientsToolStripMenuItem.Name = "clientsToolStripMenuItem";
            this.clientsToolStripMenuItem.Size = new System.Drawing.Size(198, 26);
            this.clientsToolStripMenuItem.Text = "Клиенти";
            this.clientsToolStripMenuItem.Click += new System.EventHandler(this.clientsToolStripMenuItem_Click);
            // 
            // employeesReportToolStripMenuItem
            // 
            this.employeesReportToolStripMenuItem.Name = "employeesReportToolStripMenuItem";
            this.employeesReportToolStripMenuItem.Size = new System.Drawing.Size(198, 26);
            this.employeesReportToolStripMenuItem.Text = "Служители";
            this.employeesReportToolStripMenuItem.Click += new System.EventHandler(this.employeesReportToolStripMenuItem_Click);
            // 
            // settingsMenuItem
            // 
            this.settingsMenuItem.Name = "settingsMenuItem";
            this.settingsMenuItem.Size = new System.Drawing.Size(96, 24);
            this.settingsMenuItem.Text = "Настройки";
            // 
            // helpMenuItem
            // 
            this.helpMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem,
            this.aboutMenuItem});
            this.helpMenuItem.Name = "helpMenuItem";
            this.helpMenuItem.Size = new System.Drawing.Size(73, 24);
            this.helpMenuItem.Text = "Помощ";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
            this.helpToolStripMenuItem.Text = "Помощ";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // aboutMenuItem
            // 
            this.aboutMenuItem.Name = "aboutMenuItem";
            this.aboutMenuItem.Size = new System.Drawing.Size(187, 26);
            this.aboutMenuItem.Text = "За програмата";
            this.aboutMenuItem.Click += new System.EventHandler(this.aboutMenuItem_Click);
            // 
            // mainDataGridView
            // 
            this.mainDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.8F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mainDataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.mainDataGridView.Location = new System.Drawing.Point(0, 31);
            this.mainDataGridView.Name = "mainDataGridView";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mainDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.mainDataGridView.RowTemplate.Height = 24;
            this.mainDataGridView.Size = new System.Drawing.Size(1067, 510);
            this.mainDataGridView.TabIndex = 1;
            // 
            // mainGrBox
            // 
            this.mainGrBox.Controls.Add(this.mainDataGridView);
            this.mainGrBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mainGrBox.Location = new System.Drawing.Point(0, 46);
            this.mainGrBox.Name = "mainGrBox";
            this.mainGrBox.Size = new System.Drawing.Size(1067, 541);
            this.mainGrBox.TabIndex = 2;
            this.mainGrBox.TabStop = false;
            this.mainGrBox.SizeChanged += new System.EventHandler(this.mainGrBox_SizeChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 586);
            this.Controls.Add(this.mainGrBox);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "Автомобилен сервиз";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGridView)).EndInit();
            this.mainGrBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem dataBaseMenuItem;
        private System.Windows.Forms.ToolStripMenuItem carsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sparePartsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem repairCardsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviseCardsToolStripMenuItem;
        private System.Windows.Forms.DataGridView mainDataGridView;
        private System.Windows.Forms.ToolStripMenuItem carsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeesReportToolStripMenuItem;
        private System.Windows.Forms.GroupBox mainGrBox;
    }
}

