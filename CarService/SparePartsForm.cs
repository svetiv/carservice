﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarService
{
    public partial class SparePartsForm : Form
    {
        public SparePartsForm()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DatabaseInteraction dbi = new DatabaseInteraction();
            string command = string.Format("INSERT INTO SPARE_PARTS " +
                "(PART_NUMBER, CATEGORY, NAME, DESCRIPTION, PRICE) " +
             "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", cbSpPartNumber.Text,
             cbSpPartCategory.Text, cbSpPartName.Text, txtBoxSpPartDescr.Text, txtSpPartPrice.Text);
            try
            {
                int rowUpdated = dbi.WriteData(command);
                MessageBox.Show(string.Format("Успешно добавяне на запис"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SparePartsForm_Load(object sender, EventArgs e)
        {
            DatabaseInteraction dbi = new DatabaseInteraction();
            string command = "SELECT PART_NUMBER FROM SPARE_PARTS";
            DataTable dt = dbi.ReadData("SPARE_PARTS", command);
            foreach (DataRow row in dt.Rows)
            {
                cbSpPartNumber.Items.Add(row.ItemArray[0]);
            }
            command = "SELECT CATEGORY FROM SPARE_PARTS";
            dt = dbi.ReadData("SPARE_PARTS", command);
            foreach (DataRow row in dt.Rows)
            {
                cbSpPartCategory.Items.Add(row.ItemArray[0]);
            }
            command = "SELECT NAME FROM SPARE_PARTS";
            dt = dbi.ReadData("SPARE_PARTS", command);
            foreach (DataRow row in dt.Rows)
            {
                cbSpPartName.Items.Add(row.ItemArray[0]);
            }
        }

        private void cbSpPartNumber_TextChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!string.IsNullOrEmpty(cb.Text))
            {
                DatabaseInteraction dbi = new DatabaseInteraction();
                string command = string.Format("SELECT * FROM SPARE_PARTS WHERE PART_NUMBER = {0}",
                    cb.Text);
                DataTable dt = dbi.ReadData("SPARE_PARTS", command);
                if (dt.Rows.Count != 0)
                {
                    cbSpPartNumber.Text = dt.Rows[0].ItemArray[0].ToString();
                    cbSpPartCategory.Text = dt.Rows[0].ItemArray[1].ToString();
                    cbSpPartName.Text = dt.Rows[0].ItemArray[2].ToString();
                    txtBoxSpPartDescr.Text = dt.Rows[0].ItemArray[3].ToString();
                    txtSpPartPrice.Text = dt.Rows[0].ItemArray[4].ToString();
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

        }
    }
}
