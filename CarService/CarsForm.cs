﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace CarService
{
    public partial class CarsForm : Form
    {
        string btnTxt;
        public CarsForm()
        {
            InitializeComponent();

            PopulateForm();
            ReadOnly(true);

        }

        private void ReadOnly(bool isReadOnly)
        {
            List<TextBox> txtBoxes = GetTextBoxControls();
            foreach (Control item in txtBoxes)
            {
                TextBox tb = (TextBox)item;
                if (tb.Name != "txtCarsSearchNumber")
                {
                    tb.ReadOnly = isReadOnly;
                }
            }
        }

        private void PopulateForm()
        {
            lblCarsNote.Visible = false;
            string command = "SELECT CARS.ID, REG_NUMBER, BRAND.BRAND, BRAND.MODEL, YEAR_OF_PROD, " +
                "ENGINE_NUMBER, VIN, COLOR, ENGINE_CAPACITY, DESCRIPTION, " +
                "(OWNERS.FIRST_NAME || ' ' || OWNERS.LAST_NAME) AS NAME, OWNERS.EGN, OWNERS.TELEPHONE " +
                "FROM CARS " +
                "JOIN BRAND ON CARS.BRAND_ID = BRAND.ID " +
                "JOIN OWNERS ON CARS.OWNER = OWNERS.ID";
            //List<Control> c = GetTextBoxControls();
            try
            {
                DatabaseInteraction read = new DatabaseInteraction();
                DataTable carData = new DataTable();
                carData = read.ReadData("CARS", command);
                int count = carData.Rows.Count;
                txtCarsRecordNumber.Text = carData.Rows[count - 1].ItemArray[0].ToString();
                txtCarsSearchNumber.Text = carData.Rows[count - 1].ItemArray[1].ToString();
                txtCarsRegNumber.Text = carData.Rows[count - 1].ItemArray[1].ToString();
                txtCarsBrand.Text = carData.Rows[count - 1].ItemArray[2].ToString();
                txtCarsModel.Text = carData.Rows[count - 1].ItemArray[3].ToString();
                txtCarsProdYear.Text = carData.Rows[count - 1].ItemArray[4].ToString();
                txtCarsEngineNumber.Text = carData.Rows[count - 1].ItemArray[5].ToString();
                txtCarsVIN.Text = carData.Rows[count - 1].ItemArray[6].ToString();
                txtCarsColor.Text = carData.Rows[count - 1].ItemArray[7].ToString();
                txtCarsEnginCapac.Text = carData.Rows[count - 1].ItemArray[8].ToString();
                txtCarsDescr.Text = carData.Rows[count - 1].ItemArray[9].ToString();
                txtCarsOwnerName.Text = carData.Rows[count - 1].ItemArray[10].ToString();
                txtCarsEGN.Text = carData.Rows[count - 1].ItemArray[11].ToString();
                txtCarsOwnerTelph.Text = carData.Rows[count - 1].ItemArray[12].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.GetType().ToString());
            }
        }

        private void PopulateForm(string regNumber)
        {
            string command = "SELECT CARS.ID, REG_NUMBER, BRAND.BRAND, BRAND.MODEL, YEAR_OF_PROD, " +
                        "ENGINE_NUMBER, VIN, COLOR, ENGINE_CAPACITY, DESCRIPTION, " +
                        "(OWNERS.FIRST_NAME || ' ' || OWNERS.LAST_NAME) AS NAME, OWNERS.EGN, OWNERS.TELEPHONE " +
                        "FROM CARS " +
                        "JOIN BRAND ON CARS.BRAND_ID = BRAND.ID " +
                        "JOIN OWNERS ON CARS.OWNER = OWNERS.ID " +
                        "WHERE CARS.REG_NUMBER = '" + regNumber + "'";
            try
            {
                DatabaseInteraction read = new DatabaseInteraction();
                DataTable carData = new DataTable();
                carData = read.ReadData("CARS", command);
                txtCarsRecordNumber.Text = carData.Rows[0].ItemArray[0].ToString();
                txtCarsSearchNumber.Text = carData.Rows[0].ItemArray[1].ToString();
                txtCarsRegNumber.Text = carData.Rows[0].ItemArray[1].ToString();
                txtCarsBrand.Text = carData.Rows[0].ItemArray[2].ToString();
                txtCarsModel.Text = carData.Rows[0].ItemArray[3].ToString();
                txtCarsProdYear.Text = carData.Rows[0].ItemArray[4].ToString();
                txtCarsEngineNumber.Text = carData.Rows[0].ItemArray[5].ToString();
                txtCarsVIN.Text = carData.Rows[0].ItemArray[6].ToString();
                txtCarsColor.Text = carData.Rows[0].ItemArray[7].ToString();
                txtCarsEnginCapac.Text = carData.Rows[0].ItemArray[8].ToString();
                txtCarsDescr.Text = carData.Rows[0].ItemArray[9].ToString();
                txtCarsOwnerName.Text = carData.Rows[0].ItemArray[10].ToString();
                txtCarsEGN.Text = carData.Rows[0].ItemArray[11].ToString();
                txtCarsOwnerTelph.Text = carData.Rows[0].ItemArray[12].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.GetType().ToString());
            }
        }

        private List<TextBox> GetTextBoxControls()
        {
            int count = Controls.Count;
            List<TextBox> txtBoxes = new List<TextBox>();
            for (int i = 0; i < count; i++)
            {
                //bool hasChild = Controls[i].HasChildren;
                if (Controls[i].HasChildren)
                {
                    for (int j = 0; j < Controls[i].Controls.Count; j++)
                    {
                        if (Controls[i].Controls[j].GetType() == typeof(TextBox))
                        {
                            txtBoxes.Add((TextBox)Controls[i].Controls[j]);
                        }
                    }
                }
                else
                {
                    if (Controls[i].GetType() == typeof(TextBox))
                    {
                        txtBoxes.Add((TextBox)Controls[i]);
                    }
                }
            }
            txtBoxes.Reverse();
            return txtBoxes;
        }

        private void Save(string command)
        {
            DataTable dt = new DataTable();
            DatabaseInteraction dbi = new DatabaseInteraction();
            dt = dbi.ReadData("BRAND", string.Format("SELECT BRAND FROM BRAND WHERE BRAND = '{0}'", txtCarsBrand.Text.ToUpper()));
            if (dt.Rows.Count == 0)
            {
                dbi.WriteData(string.Format("INSERT INTO BRAND(ID, BRAND, MODEL) VALUES(BRAND_ID.NEXTVAL, '{0}', '{1}')",
                    txtCarsBrand.Text.ToUpper(), txtCarsModel.Text.ToUpper()));
            }
            dt = dbi.ReadData("BRAND", string.Format("SELECT BRAND FROM BRAND WHERE MODEL = '{0}'", txtCarsModel.Text.ToUpper()));
            if (dt.Rows.Count == 0)
            {
                dbi.WriteData(string.Format("INSERT INTO BRAND(ID, BRAND, MODEL) VALUES(BRAND_ID.NEXTVAL, '{0}', '{1}')",
                    txtCarsBrand.Text.ToUpper(), txtCarsModel.Text.ToUpper()));
            }

            dt = dbi.ReadData("OWNERS", string.Format("SELECT EGN FROM OWNERS WHERE EGN = '{0}'",
                txtCarsEGN.Text.ToUpper()));
            if (dt.Rows.Count == 0)
            {
                string[] str = txtCarsOwnerName.Text.Split(' ');
                dbi.WriteData(string.Format(
                    "INSERT INTO OWNERS(ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, EGN, TELEPHONE) " +
                    "VALUES(CLIENT_ID.NEXTVAL, '{0}', '{1}', '{2}', '{3}', '{4}')",
                    str[0].ToUpper(),
                    str.Length == 2 ? null : str[1].ToUpper(),
                    str.Length == 2 ? str[1].ToUpper() : str[2].ToUpper(),
                    txtCarsEGN.Text,
                    txtCarsOwnerTelph.Text));
            }
            dt = dbi.ReadData("BRAND", string.Format("SELECT ID FROM BRAND WHERE BRAND = '{0}' AND MODEL = '{1}'",
                txtCarsBrand.Text.ToUpper(), txtCarsModel.Text.ToUpper()));
            string brandID = dt.Rows[0].ItemArray[0].ToString();

            dbi = new DatabaseInteraction();
            try
            {
                dbi.WriteData(command);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.GetType().ToString());
            }
        }
        private void btnCarsNew_Click(object sender, EventArgs e)
        {
            ReadOnly(false);
            lblCarsNote.Visible = true;
            List<TextBox> txtBoxes = GetTextBoxControls();
            if (btnCarsNew.Text == "Нов запис")
            {
                foreach (TextBox item in txtBoxes)
                {
                    item.Text = "";
                }
                btnCarsNew.Text = "Запази";
                btnCarsEdit.Text = "Отказ";
                btnCarsDelete.Visible = false;
                btnTxt = "Нов запис";
            }
            else if (btnCarsNew.Text == "Запази")
            {
                switch (btnTxt)
                {
                    case "Нов запис":
                        string command = string.Format(
                        "INSERT INTO CARS(ID, REG_NUMBER, BRAND_ID, YEAR_OF_PROD, ENGINE_NUMBER, " +
                        "VIN, COLOR, ENGINE_CAPACITY, DESCRIPTION, OWNER) " +
                        "VALUES(CARS_ID.NEXTVAL, '{0}', " +
                        "(SELECT ID FROM BRAND WHERE BRAND = '{1}' AND MODEL = '{2}'), " +
                        "'{3}', '{4}', '{5}', '{6}', '{7}', '{8}', " +
                        "(SELECT ID FROM OWNERS WHERE OWNERS.EGN = '{9}'))",
                        txtCarsRegNumber.Text, txtCarsBrand.Text.ToUpper(), txtCarsModel.Text.ToUpper(),
                        txtCarsProdYear.Text,
                        txtCarsEngineNumber.Text, txtCarsVIN.Text, txtCarsColor.Text, txtCarsEnginCapac.Text,
                        txtCarsDescr.Text, txtCarsEGN.Text);
                        Save(command);
                        break;
                    case "Редактиране":
                        command = string.Format(
                        "UPDATE CARS SET CARS.REG_NUMBER = '{0}', " +
                        "BRAND_ID = (SELECT ID FROM BRAND WHERE BRAND.BRAND = '{1}' AND BRAND.MODEL = '{2}'), " +
                        "YEAR_OF_PROD = '{3}', ENGINE_NUMBER = '{4}', VIN = '{5}', COLOR = '{6}', " +
                        "ENGINE_CAPACITY = '{7}', DESCRIPTION = '{8}', " +
                        "OWNER = (SELECT ID FROM OWNERS WHERE OWNERS.EGN = '{9}') " +
                        "WHERE CARS.REG_NUMBER = '{10}'",
                        txtCarsRegNumber.Text, txtCarsBrand.Text, txtCarsModel.Text, txtCarsProdYear.Text,
                        txtCarsEngineNumber.Text, txtCarsVIN.Text, txtCarsColor.Text, txtCarsEnginCapac.Text,
                        txtCarsDescr.Text, txtCarsEGN.Text, txtCarsSearchNumber.Text);
                        //command = string.Format(
                        //    "UPDATE CARS SET REG_NUMBER = '{0}', YEAR_OF_PROD = '{1}', COLOR = '{2}' " +
                        //    "WHERE CARS.REG_NUMBER = '{3}'",
                        //    txtCarsRegNumber.Text, txtCarsProdYear.Text, 
                        //    txtCarsColor.Text, txtCarsSearchNumber.Text);
                        Save(command);
                        break;
                }
            }
        }

        private void btnCarsEdit_Click(object sender, EventArgs e)
        {
            if (btnCarsEdit.Text == "Редактиране")
            {
                btnCarsNew.Text = "Запази";
                btnCarsEdit.Text = "Отказ";
                btnCarsDelete.Visible = false;
                ReadOnly(false);
                btnTxt = "Редактиране";
            }
            else if (btnCarsEdit.Text == "Отказ")
            {
                btnCarsNew.Text = "Нов запис";
                btnCarsEdit.Text = "Редактиране";
                btnCarsDelete.Visible = true;
                PopulateForm();
                ReadOnly(true);
            }
        }

        private void btnCarsSearch_Click(object sender, EventArgs e)
        {
            PopulateForm(txtCarsSearchNumber.Text);
        }

        private void btnCarsDelete_Click(object sender, EventArgs e)
        {
            string command = string.Format("DELETE FROM CARS WHERE CARS.REG_NUMBER = '{0}'",
                txtCarsRegNumber.Text);
            Save(command);
            PopulateForm();
        }
    }
}
