﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarService
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void carsMenuItem_Click(object sender, EventArgs e)
        {
            CarsForm cars = new CarsForm();
            cars.StartPosition = FormStartPosition.CenterScreen;
            cars.Show(this);
        }

        private void sparePartsMenuItem_Click(object sender, EventArgs e)
        {
            SparePartsForm spareParts = new SparePartsForm();
            spareParts.StartPosition = FormStartPosition.CenterScreen;
            spareParts.Show(this);
        }

        private void repairCardsMenuItem_Click(object sender, EventArgs e)
        {
            RepairCardsForm repairCards = new RepairCardsForm();
            repairCards.StartPosition = FormStartPosition.CenterScreen;
            repairCards.Show(this);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            GrBoxResize();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string path = Directory.GetCurrentDirectory();
            string[] files = Directory.GetFiles(path);
            string file = "";
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Contains("pdf"))
                {
                    file = files[i];
                }
            }
            System.Diagnostics.Process.Start(file);
        }

        private void aboutMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.StartPosition = FormStartPosition.CenterScreen;
            ab.Show(this);
        }

        private void employeesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Employees empl = new Employees();
            empl.StartPosition = FormStartPosition.CenterParent;
            empl.ShowDialog();
        }

        private void carsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainGrBox.Text = "Справка Автомобили";
            string command = string.Format(
                "SELECT REG_NUMBER AS РЕГ_НОМЕР, BRAND.BRAND AS МАРКА, BRAND.MODEL AS МОДЕЛ, " +
                "OWNERS.FIRST_NAME AS ИМЕ, OWNERS.LAST_NAME AS ФАМИЛИЯ, OWNERS.TELEPHONE AS ТЕЛЕФОН " +
                "FROM CARS " +
                "JOIN BRAND ON BRAND.ID = CARS.BRAND_ID " +
                "JOIN OWNERS ON OWNERS.ID = CARS.OWNER");
            Report("CARS", command);
        }

        private void Report(string table, string command)
        {
            mainDataGridView.DataSource = null;
            DatabaseInteraction dbi = new DatabaseInteraction();
            DataTable dt = new DataTable();
            try
            {
                dt = dbi.ReadData(table, command);
                mainDataGridView.DataSource = dt;
                mainDataGridView.EditMode = DataGridViewEditMode.EditOnEnter;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void clientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainGrBox.Text = "Справка Клиенти";
            string command = string.Format(
               "SELECT OWNERS.FIRST_NAME AS ИМЕ, OWNERS.MIDDLE_NAME AS ПРЕЗИМЕ, " +
               "OWNERS.LAST_NAME AS ФАМИЛИЯ, OWNERS.EGN AS ЕГН, OWNERS.TELEPHONE AS ТЕЛЕФОН " +
               "FROM OWNERS");
            Report("OWNERS", command);
        }

        private void serviceCardsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainGrBox.Text = "Справка Ремонтни Карти";
            string command = "SELECT CARD_NUMBER AS НОМЕР, ACC_DATE AS \"ДАТА НА ПРИЕМАНЕ\", " +
                "REL_DATE AS \"ДАТА НА ИЗДАВАНЕ\", CARS.REG_NUMBER AS \"РЕГИСТР. НОМЕР\", " +
                "SERVICE_CARDS.DESCRIPTION AS \"ОПИС. НА РЕМОНТА\", " +
                "(EMPLOYEES.FIRST_NAME || ' ' || EMPLOYEES.LAST_NAME) AS \"ИМЕ НА СЛУЖИТЕЛ\", SP_PARTS_NUMBER, PR_SP_PARTS, " +
                "PR_LABOR, PR_TOTAL " +
                "FROM SERVICE_CARDS " +
                "JOIN CARS ON CARS.REG_NUMBER = SERVICE_CARDS.REG_NUMBER " +
                "JOIN EMPLOYEES ON EMPLOYEES.ID = SERVICE_CARDS.EMPLOYEE_ID";
            Report("SERVICE_CARDS", command);
        }

        private void employeesReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainGrBox.Text = "Справка Служители";
            string command = "SELECT * FROM EMPLOYEES";
            Report("EEMPLOYEES", command);
        }

        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            GrBoxResize();
        }

        private void mainGrBox_SizeChanged(object sender, EventArgs e)
        {
            mainDataGridView.Width = mainGrBox.Width;
            mainDataGridView.Height = mainGrBox.Height;
        }

        private void GrBoxResize()
        {
            mainGrBox.Width = this.Width - 16;
            mainGrBox.Height = this.Height - 77;
        }
    }
}
