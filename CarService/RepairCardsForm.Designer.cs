﻿namespace CarService
{
    partial class RepairCardsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRepCardsNumber = new System.Windows.Forms.Label();
            this.lblRepCardsStartDate = new System.Windows.Forms.Label();
            this.lblRRepCardsEndDate = new System.Windows.Forms.Label();
            this.lblRepCardsCars = new System.Windows.Forms.Label();
            this.lblRepCardsDescr = new System.Windows.Forms.Label();
            this.lblRepCardsEmpl = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRepCardsNumber = new System.Windows.Forms.TextBox();
            this.txtRepCardsDescr = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.startDate = new System.Windows.Forms.DateTimePicker();
            this.endDate = new System.Windows.Forms.DateTimePicker();
            this.btnCardsNew = new System.Windows.Forms.Button();
            this.btnCardsEdit = new System.Windows.Forms.Button();
            this.btnCardsDelete = new System.Windows.Forms.Button();
            this.lstRepCardsRepParts = new System.Windows.Forms.ListBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comBoxRepCardsCars = new System.Windows.Forms.ComboBox();
            this.comBoxRepCardsEpmpl = new System.Windows.Forms.ComboBox();
            this.btnRepCardsAdd = new System.Windows.Forms.Button();
            this.btnRepCardsDelete = new System.Windows.Forms.Button();
            this.lblRepCardsRepParts = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblRepCardsNumber
            // 
            this.lblRepCardsNumber.AutoSize = true;
            this.lblRepCardsNumber.Location = new System.Drawing.Point(20, 25);
            this.lblRepCardsNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRepCardsNumber.Name = "lblRepCardsNumber";
            this.lblRepCardsNumber.Size = new System.Drawing.Size(167, 17);
            this.lblRepCardsNumber.TabIndex = 0;
            this.lblRepCardsNumber.Text = "№ на ремонтната карта";
            // 
            // lblRepCardsStartDate
            // 
            this.lblRepCardsStartDate.AutoSize = true;
            this.lblRepCardsStartDate.Location = new System.Drawing.Point(229, 25);
            this.lblRepCardsStartDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRepCardsStartDate.Name = "lblRepCardsStartDate";
            this.lblRepCardsStartDate.Size = new System.Drawing.Size(131, 17);
            this.lblRepCardsStartDate.TabIndex = 1;
            this.lblRepCardsStartDate.Text = "Дата на приемане";
            // 
            // lblRRepCardsEndDate
            // 
            this.lblRRepCardsEndDate.AutoSize = true;
            this.lblRRepCardsEndDate.Location = new System.Drawing.Point(431, 25);
            this.lblRRepCardsEndDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRRepCardsEndDate.Name = "lblRRepCardsEndDate";
            this.lblRRepCardsEndDate.Size = new System.Drawing.Size(128, 17);
            this.lblRRepCardsEndDate.TabIndex = 2;
            this.lblRRepCardsEndDate.Text = "Дата на излизане";
            // 
            // lblRepCardsCars
            // 
            this.lblRepCardsCars.AutoSize = true;
            this.lblRepCardsCars.Location = new System.Drawing.Point(20, 103);
            this.lblRepCardsCars.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRepCardsCars.Name = "lblRepCardsCars";
            this.lblRepCardsCars.Size = new System.Drawing.Size(80, 17);
            this.lblRepCardsCars.TabIndex = 3;
            this.lblRepCardsCars.Text = "Автомобил";
            // 
            // lblRepCardsDescr
            // 
            this.lblRepCardsDescr.AutoSize = true;
            this.lblRepCardsDescr.Location = new System.Drawing.Point(20, 173);
            this.lblRepCardsDescr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRepCardsDescr.Name = "lblRepCardsDescr";
            this.lblRepCardsDescr.Size = new System.Drawing.Size(154, 17);
            this.lblRepCardsDescr.TabIndex = 4;
            this.lblRepCardsDescr.Text = "Описание на ремонта";
            // 
            // lblRepCardsEmpl
            // 
            this.lblRepCardsEmpl.AutoSize = true;
            this.lblRepCardsEmpl.Location = new System.Drawing.Point(312, 103);
            this.lblRepCardsEmpl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRepCardsEmpl.Name = "lblRepCardsEmpl";
            this.lblRepCardsEmpl.Size = new System.Drawing.Size(72, 17);
            this.lblRepCardsEmpl.TabIndex = 5;
            this.lblRepCardsEmpl.Text = "Служител";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(345, 543);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Цена на труд";
            // 
            // txtRepCardsNumber
            // 
            this.txtRepCardsNumber.Location = new System.Drawing.Point(24, 44);
            this.txtRepCardsNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtRepCardsNumber.Name = "txtRepCardsNumber";
            this.txtRepCardsNumber.Size = new System.Drawing.Size(196, 22);
            this.txtRepCardsNumber.TabIndex = 8;
            // 
            // txtRepCardsDescr
            // 
            this.txtRepCardsDescr.Location = new System.Drawing.Point(24, 194);
            this.txtRepCardsDescr.Margin = new System.Windows.Forms.Padding(4);
            this.txtRepCardsDescr.Multiline = true;
            this.txtRepCardsDescr.Name = "txtRepCardsDescr";
            this.txtRepCardsDescr.Size = new System.Drawing.Size(602, 163);
            this.txtRepCardsDescr.TabIndex = 13;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(450, 538);
            this.textBox6.Margin = new System.Windows.Forms.Padding(4);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(176, 22);
            this.textBox6.TabIndex = 15;
            // 
            // startDate
            // 
            this.startDate.CustomFormat = "ddd dd-MMM-yyyy HH:mm:ss";
            this.startDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startDate.Location = new System.Drawing.Point(229, 44);
            this.startDate.Margin = new System.Windows.Forms.Padding(4);
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(197, 22);
            this.startDate.TabIndex = 16;
            this.startDate.Value = new System.DateTime(2016, 12, 18, 0, 0, 0, 0);
            // 
            // endDate
            // 
            this.endDate.CustomFormat = "ddd dd-MMM-yyyy HH:mm:ss";
            this.endDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endDate.Location = new System.Drawing.Point(434, 44);
            this.endDate.Margin = new System.Windows.Forms.Padding(4);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(192, 22);
            this.endDate.TabIndex = 17;
            // 
            // btnCardsNew
            // 
            this.btnCardsNew.Location = new System.Drawing.Point(293, 676);
            this.btnCardsNew.Margin = new System.Windows.Forms.Padding(4);
            this.btnCardsNew.Name = "btnCardsNew";
            this.btnCardsNew.Size = new System.Drawing.Size(100, 28);
            this.btnCardsNew.TabIndex = 18;
            this.btnCardsNew.Text = "Нов запис";
            this.btnCardsNew.UseVisualStyleBackColor = true;
            this.btnCardsNew.Click += new System.EventHandler(this.btnCardsNew_Click);
            // 
            // btnCardsEdit
            // 
            this.btnCardsEdit.Location = new System.Drawing.Point(401, 676);
            this.btnCardsEdit.Margin = new System.Windows.Forms.Padding(4);
            this.btnCardsEdit.Name = "btnCardsEdit";
            this.btnCardsEdit.Size = new System.Drawing.Size(107, 28);
            this.btnCardsEdit.TabIndex = 19;
            this.btnCardsEdit.Text = "Редактиране";
            this.btnCardsEdit.UseVisualStyleBackColor = true;
            // 
            // btnCardsDelete
            // 
            this.btnCardsDelete.Location = new System.Drawing.Point(526, 676);
            this.btnCardsDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnCardsDelete.Name = "btnCardsDelete";
            this.btnCardsDelete.Size = new System.Drawing.Size(100, 28);
            this.btnCardsDelete.TabIndex = 20;
            this.btnCardsDelete.Text = "Изтриване";
            this.btnCardsDelete.UseVisualStyleBackColor = true;
            // 
            // lstRepCardsRepParts
            // 
            this.lstRepCardsRepParts.FormattingEnabled = true;
            this.lstRepCardsRepParts.ItemHeight = 16;
            this.lstRepCardsRepParts.Location = new System.Drawing.Point(23, 396);
            this.lstRepCardsRepParts.Name = "lstRepCardsRepParts";
            this.lstRepCardsRepParts.Size = new System.Drawing.Size(532, 116);
            this.lstRepCardsRepParts.TabIndex = 21;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(23, 682);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(176, 22);
            this.textBox5.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 662);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 17);
            this.label9.TabIndex = 23;
            this.label9.Text = "№ на запис";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(450, 568);
            this.textBox7.Margin = new System.Windows.Forms.Padding(4);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(176, 22);
            this.textBox7.TabIndex = 24;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(450, 596);
            this.textBox8.Margin = new System.Windows.Forms.Padding(4);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(176, 22);
            this.textBox8.TabIndex = 25;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(310, 599);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 17);
            this.label10.TabIndex = 26;
            this.label10.Text = "Обща цена с ДДС ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(271, 571);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(171, 17);
            this.label11.TabIndex = 27;
            this.label11.Text = "Цена на резервни части";
            // 
            // comBoxRepCardsCars
            // 
            this.comBoxRepCardsCars.FormattingEnabled = true;
            this.comBoxRepCardsCars.Location = new System.Drawing.Point(16, 123);
            this.comBoxRepCardsCars.Name = "comBoxRepCardsCars";
            this.comBoxRepCardsCars.Size = new System.Drawing.Size(197, 24);
            this.comBoxRepCardsCars.TabIndex = 28;
            // 
            // comBoxRepCardsEpmpl
            // 
            this.comBoxRepCardsEpmpl.FormattingEnabled = true;
            this.comBoxRepCardsEpmpl.Location = new System.Drawing.Point(315, 124);
            this.comBoxRepCardsEpmpl.Name = "comBoxRepCardsEpmpl";
            this.comBoxRepCardsEpmpl.Size = new System.Drawing.Size(311, 24);
            this.comBoxRepCardsEpmpl.TabIndex = 29;
            // 
            // btnRepCardsAdd
            // 
            this.btnRepCardsAdd.Location = new System.Drawing.Point(561, 396);
            this.btnRepCardsAdd.Name = "btnRepCardsAdd";
            this.btnRepCardsAdd.Size = new System.Drawing.Size(65, 23);
            this.btnRepCardsAdd.TabIndex = 30;
            this.btnRepCardsAdd.Text = "Добави";
            this.btnRepCardsAdd.UseVisualStyleBackColor = true;
            // 
            // btnRepCardsDelete
            // 
            this.btnRepCardsDelete.Location = new System.Drawing.Point(561, 425);
            this.btnRepCardsDelete.Name = "btnRepCardsDelete";
            this.btnRepCardsDelete.Size = new System.Drawing.Size(65, 23);
            this.btnRepCardsDelete.TabIndex = 31;
            this.btnRepCardsDelete.Text = "Изтрий";
            this.btnRepCardsDelete.UseVisualStyleBackColor = true;
            // 
            // lblRepCardsRepParts
            // 
            this.lblRepCardsRepParts.AutoSize = true;
            this.lblRepCardsRepParts.Location = new System.Drawing.Point(20, 376);
            this.lblRepCardsRepParts.Name = "lblRepCardsRepParts";
            this.lblRepCardsRepParts.Size = new System.Drawing.Size(193, 17);
            this.lblRepCardsRepParts.TabIndex = 32;
            this.lblRepCardsRepParts.Text = "Списък на вложените части";
            // 
            // RepairCardsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 717);
            this.Controls.Add(this.lblRepCardsRepParts);
            this.Controls.Add(this.btnRepCardsDelete);
            this.Controls.Add(this.btnRepCardsAdd);
            this.Controls.Add(this.lstRepCardsRepParts);
            this.Controls.Add(this.comBoxRepCardsEpmpl);
            this.Controls.Add(this.comBoxRepCardsCars);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.btnCardsDelete);
            this.Controls.Add(this.btnCardsEdit);
            this.Controls.Add(this.btnCardsNew);
            this.Controls.Add(this.endDate);
            this.Controls.Add(this.startDate);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.txtRepCardsDescr);
            this.Controls.Add(this.txtRepCardsNumber);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblRepCardsEmpl);
            this.Controls.Add(this.lblRepCardsDescr);
            this.Controls.Add(this.lblRepCardsCars);
            this.Controls.Add(this.lblRRepCardsEndDate);
            this.Controls.Add(this.lblRepCardsStartDate);
            this.Controls.Add(this.lblRepCardsNumber);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "RepairCardsForm";
            this.Text = "Ремонтни карти";
            this.Load += new System.EventHandler(this.RepairCardsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRepCardsNumber;
        private System.Windows.Forms.Label lblRepCardsStartDate;
        private System.Windows.Forms.Label lblRRepCardsEndDate;
        private System.Windows.Forms.Label lblRepCardsCars;
        private System.Windows.Forms.Label lblRepCardsDescr;
        private System.Windows.Forms.Label lblRepCardsEmpl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtRepCardsNumber;
        private System.Windows.Forms.TextBox txtRepCardsDescr;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.DateTimePicker startDate;
        private System.Windows.Forms.DateTimePicker endDate;
        private System.Windows.Forms.Button btnCardsNew;
        private System.Windows.Forms.Button btnCardsEdit;
        private System.Windows.Forms.Button btnCardsDelete;
        private System.Windows.Forms.ListBox lstRepCardsRepParts;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comBoxRepCardsCars;
        private System.Windows.Forms.ComboBox comBoxRepCardsEpmpl;
        private System.Windows.Forms.Button btnRepCardsAdd;
        private System.Windows.Forms.Button btnRepCardsDelete;
        private System.Windows.Forms.Label lblRepCardsRepParts;
    }
}