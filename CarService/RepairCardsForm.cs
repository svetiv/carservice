﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarService
{
    public partial class RepairCardsForm : Form
    {
        bool isFormOpen = false;
        public RepairCardsForm()
        {
            InitializeComponent();
            isFormOpen = true;
        }
        private List<Control> GetTextBoxControls()
        {
            List<Control> c = Controls.OfType<TextBox>().Cast<Control>().ToList();
            c.Reverse();
            return c;
        }
        private void btnCardsNew_Click(object sender, EventArgs e)
        {
            List<Control> c = GetTextBoxControls();
            if (btnCardsNew.Text == "Нов запис")
            {
                foreach (var item in c)
                {
                    item.Text = "";
                }
                btnCardsNew.Text = "Запази";
                btnCardsEdit.Text = "Отказ";
                btnCardsDelete.Visible = false;
            }
        }

        private void RepairCardsForm_Load(object sender, EventArgs e)
        {
            DatabaseInteraction dbi = new DatabaseInteraction();
            string command = "SELECT REG_NUMBER FROM CARS";
            DataTable dt = dbi.ReadData("CARS", command);
            foreach (DataRow row in dt.Rows)
            {
                comBoxRepCardsCars.Items.Add(row.ItemArray[0]);
            }
            comBoxRepCardsCars.SelectedIndex = 0;
            command = "SELECT (FIRST_NAME || ' ' || LAST_NAME) AS NAME FROM EMPLOYEES";
            dt = dbi.ReadData("EMPLOYEES", command);
            foreach (DataRow row in dt.Rows)
            {
                comBoxRepCardsEpmpl.Items.Add(row.ItemArray[0]);
            }
            comBoxRepCardsEpmpl.SelectedIndex = 0;
        }
    }
}
