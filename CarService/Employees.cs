﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarService
{
    public partial class Employees : Form
    {
        DatabaseInteraction dbi = new DatabaseInteraction();
        public Employees()
        {
            InitializeComponent();
        }

        private void btnEmployeeAdd_Click(object sender, EventArgs e)
        {
            string command = string.Format(
                "INSERT INTO EMPLOYEES (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, EGN, POSITION) " +
                "VALUES (EMPLOYEE_ID.NEXTVAL, '{0}', '{1}', '{2}', '{3}', '{4}')", txtEmployeeFirstName.Text,
                txtEmployeeMiddleName.Text, txtEmployeeLastName.Text, txtEmployeeEGN.Text, txtEmployeePosition.Text);
            try
            {
                int rowUpdated = dbi.WriteData(command);
                MessageBox.Show(string.Format("Успешно добавяне на {0} {1}",
                        rowUpdated, rowUpdated == 1 ? "запис" : "записа"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEmployeeEdit_Click(object sender, EventArgs e)
        {
            string command = "";
            if (txtEmployeeID.Text == "")
            {
                string txt = txtEmployeeEGN.Text;
                command = string.Format("SELECT * FROM EMPLOYEES WHERE EGN = {0}", txt);
                DatabaseInteraction dbi = new DatabaseInteraction();
                try
                {
                    DataTable dt = dbi.ReadData("EMPLOYEES", command);
                    txtEmployeeID.Text = dt.Rows[0].ItemArray[0].ToString();
                    txtEmployeeFirstName.Text = dt.Rows[0].ItemArray[1].ToString();
                    txtEmployeeMiddleName.Text = dt.Rows[0].ItemArray[2].ToString();
                    txtEmployeeLastName.Text = dt.Rows[0].ItemArray[3].ToString();
                    txtEmployeeEGN.Text = dt.Rows[0].ItemArray[4].ToString();
                    txtEmployeePosition.Text = dt.Rows[0].ItemArray[5].ToString();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                command = string.Format(
                     "UPDATE EMPLOYEES SET FIRST_NAME = '{0}', MIDDLE_NAME = '{1}', " +
                     "LAST_NAME = '{2}', EGN = '{3}', POSITION = '{4}' " +
                     "WHERE EMPLOYEES.EGN = '{5}'",
                     txtEmployeeFirstName.Text, txtEmployeeMiddleName.Text, txtEmployeeLastName.Text,
                     txtEmployeeEGN.Text, txtEmployeePosition.Text, txtEmployeeEGN.Text);
                try
                {
                    int rowUpdated = dbi.WriteData(command);
                    MessageBox.Show(string.Format("Успешно добавяне на {0} {1}",
                        rowUpdated, rowUpdated == 1 ? "запис" : "записа"));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void btnEmployeeDelete_Click(object sender, EventArgs e)
        {
            string command = "";
            DatabaseInteraction dbi = new DatabaseInteraction();
            if (txtEmployeeID.Text == "")
            {
                string txt = txtEmployeeEGN.Text;
                command = string.Format("SELECT * FROM EMPLOYEES WHERE EGN = {0}", txt);
                try
                {
                    DataTable dt = dbi.ReadData("EMPLOYEES", command);
                    txtEmployeeID.Text = dt.Rows[0].ItemArray[0].ToString();
                    txtEmployeeFirstName.Text = dt.Rows[0].ItemArray[1].ToString();
                    txtEmployeeMiddleName.Text = dt.Rows[0].ItemArray[2].ToString();
                    txtEmployeeLastName.Text = dt.Rows[0].ItemArray[3].ToString();
                    txtEmployeeEGN.Text = dt.Rows[0].ItemArray[4].ToString();
                    txtEmployeePosition.Text = dt.Rows[0].ItemArray[5].ToString();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                command = string.Format("DELETE FROM EMPLOYEES WHERE EMPLOYEES.EGN = '{0}'",
                   txtEmployeeEGN.Text);
                try
                {
                    int rowUpdated = dbi.WriteData(command);
                    MessageBox.Show(string.Format("Успешно изтриване на {0} {1}",
                        rowUpdated, rowUpdated == 1 ? "запис" : "записа"));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
