﻿namespace CarService
{
    partial class Employees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmployeeID = new System.Windows.Forms.Label();
            this.lblEmployeeFirstName = new System.Windows.Forms.Label();
            this.lblEmployeeMiddleName = new System.Windows.Forms.Label();
            this.lblEmployeeLastName = new System.Windows.Forms.Label();
            this.lblEployeePosition = new System.Windows.Forms.Label();
            this.txtEmployeeID = new System.Windows.Forms.TextBox();
            this.txtEmployeeFirstName = new System.Windows.Forms.TextBox();
            this.txtEmployeeMiddleName = new System.Windows.Forms.TextBox();
            this.txtEmployeeLastName = new System.Windows.Forms.TextBox();
            this.txtEmployeePosition = new System.Windows.Forms.TextBox();
            this.btnEmployeeAdd = new System.Windows.Forms.Button();
            this.btnEmployeeEdit = new System.Windows.Forms.Button();
            this.btnEmployeeDelete = new System.Windows.Forms.Button();
            this.txtEmployeeEGN = new System.Windows.Forms.TextBox();
            this.lblEmployeeEGN = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblEmployeeID
            // 
            this.lblEmployeeID.AutoSize = true;
            this.lblEmployeeID.Location = new System.Drawing.Point(12, 24);
            this.lblEmployeeID.Name = "lblEmployeeID";
            this.lblEmployeeID.Size = new System.Drawing.Size(22, 17);
            this.lblEmployeeID.TabIndex = 0;
            this.lblEmployeeID.Text = "№";
            // 
            // lblEmployeeFirstName
            // 
            this.lblEmployeeFirstName.AutoSize = true;
            this.lblEmployeeFirstName.Location = new System.Drawing.Point(12, 52);
            this.lblEmployeeFirstName.Name = "lblEmployeeFirstName";
            this.lblEmployeeFirstName.Size = new System.Drawing.Size(40, 17);
            this.lblEmployeeFirstName.TabIndex = 1;
            this.lblEmployeeFirstName.Text = "Име*";
            // 
            // lblEmployeeMiddleName
            // 
            this.lblEmployeeMiddleName.AutoSize = true;
            this.lblEmployeeMiddleName.Location = new System.Drawing.Point(12, 80);
            this.lblEmployeeMiddleName.Name = "lblEmployeeMiddleName";
            this.lblEmployeeMiddleName.Size = new System.Drawing.Size(66, 17);
            this.lblEmployeeMiddleName.TabIndex = 2;
            this.lblEmployeeMiddleName.Text = "Презиме";
            // 
            // lblEmployeeLastName
            // 
            this.lblEmployeeLastName.AutoSize = true;
            this.lblEmployeeLastName.Location = new System.Drawing.Point(12, 108);
            this.lblEmployeeLastName.Name = "lblEmployeeLastName";
            this.lblEmployeeLastName.Size = new System.Drawing.Size(75, 17);
            this.lblEmployeeLastName.TabIndex = 3;
            this.lblEmployeeLastName.Text = "Фамилия*";
            // 
            // lblEployeePosition
            // 
            this.lblEployeePosition.AutoSize = true;
            this.lblEployeePosition.Location = new System.Drawing.Point(12, 164);
            this.lblEployeePosition.Name = "lblEployeePosition";
            this.lblEployeePosition.Size = new System.Drawing.Size(75, 17);
            this.lblEployeePosition.TabIndex = 4;
            this.lblEployeePosition.Text = "Длъжност";
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.Location = new System.Drawing.Point(96, 21);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.ReadOnly = true;
            this.txtEmployeeID.Size = new System.Drawing.Size(174, 22);
            this.txtEmployeeID.TabIndex = 5;
            // 
            // txtEmployeeFirstName
            // 
            this.txtEmployeeFirstName.Location = new System.Drawing.Point(96, 49);
            this.txtEmployeeFirstName.Name = "txtEmployeeFirstName";
            this.txtEmployeeFirstName.Size = new System.Drawing.Size(174, 22);
            this.txtEmployeeFirstName.TabIndex = 6;
            // 
            // txtEmployeeMiddleName
            // 
            this.txtEmployeeMiddleName.Location = new System.Drawing.Point(96, 77);
            this.txtEmployeeMiddleName.Name = "txtEmployeeMiddleName";
            this.txtEmployeeMiddleName.Size = new System.Drawing.Size(174, 22);
            this.txtEmployeeMiddleName.TabIndex = 7;
            // 
            // txtEmployeeLastName
            // 
            this.txtEmployeeLastName.Location = new System.Drawing.Point(96, 105);
            this.txtEmployeeLastName.Name = "txtEmployeeLastName";
            this.txtEmployeeLastName.Size = new System.Drawing.Size(174, 22);
            this.txtEmployeeLastName.TabIndex = 8;
            // 
            // txtEmployeePosition
            // 
            this.txtEmployeePosition.Location = new System.Drawing.Point(96, 161);
            this.txtEmployeePosition.Name = "txtEmployeePosition";
            this.txtEmployeePosition.Size = new System.Drawing.Size(174, 22);
            this.txtEmployeePosition.TabIndex = 9;
            // 
            // btnEmployeeAdd
            // 
            this.btnEmployeeAdd.Location = new System.Drawing.Point(12, 249);
            this.btnEmployeeAdd.Name = "btnEmployeeAdd";
            this.btnEmployeeAdd.Size = new System.Drawing.Size(75, 23);
            this.btnEmployeeAdd.TabIndex = 10;
            this.btnEmployeeAdd.Text = "Добави";
            this.btnEmployeeAdd.UseVisualStyleBackColor = true;
            this.btnEmployeeAdd.Click += new System.EventHandler(this.btnEmployeeAdd_Click);
            // 
            // btnEmployeeEdit
            // 
            this.btnEmployeeEdit.Location = new System.Drawing.Point(93, 249);
            this.btnEmployeeEdit.Name = "btnEmployeeEdit";
            this.btnEmployeeEdit.Size = new System.Drawing.Size(96, 23);
            this.btnEmployeeEdit.TabIndex = 11;
            this.btnEmployeeEdit.Text = "Редактиране";
            this.btnEmployeeEdit.UseVisualStyleBackColor = true;
            this.btnEmployeeEdit.Click += new System.EventHandler(this.btnEmployeeEdit_Click);
            // 
            // btnEmployeeDelete
            // 
            this.btnEmployeeDelete.Location = new System.Drawing.Point(195, 249);
            this.btnEmployeeDelete.Name = "btnEmployeeDelete";
            this.btnEmployeeDelete.Size = new System.Drawing.Size(75, 23);
            this.btnEmployeeDelete.TabIndex = 12;
            this.btnEmployeeDelete.Text = "Изтрий";
            this.btnEmployeeDelete.UseVisualStyleBackColor = true;
            this.btnEmployeeDelete.Click += new System.EventHandler(this.btnEmployeeDelete_Click);
            // 
            // txtEmployeeEGN
            // 
            this.txtEmployeeEGN.Location = new System.Drawing.Point(96, 133);
            this.txtEmployeeEGN.Name = "txtEmployeeEGN";
            this.txtEmployeeEGN.Size = new System.Drawing.Size(174, 22);
            this.txtEmployeeEGN.TabIndex = 13;
            // 
            // lblEmployeeEGN
            // 
            this.lblEmployeeEGN.AutoSize = true;
            this.lblEmployeeEGN.Location = new System.Drawing.Point(12, 138);
            this.lblEmployeeEGN.Name = "lblEmployeeEGN";
            this.lblEmployeeEGN.Size = new System.Drawing.Size(40, 17);
            this.lblEmployeeEGN.TabIndex = 14;
            this.lblEmployeeEGN.Text = "ЕГН*";
            // 
            // Employees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 284);
            this.Controls.Add(this.lblEmployeeEGN);
            this.Controls.Add(this.txtEmployeeEGN);
            this.Controls.Add(this.btnEmployeeDelete);
            this.Controls.Add(this.btnEmployeeEdit);
            this.Controls.Add(this.btnEmployeeAdd);
            this.Controls.Add(this.txtEmployeePosition);
            this.Controls.Add(this.txtEmployeeLastName);
            this.Controls.Add(this.txtEmployeeMiddleName);
            this.Controls.Add(this.txtEmployeeFirstName);
            this.Controls.Add(this.txtEmployeeID);
            this.Controls.Add(this.lblEployeePosition);
            this.Controls.Add(this.lblEmployeeLastName);
            this.Controls.Add(this.lblEmployeeMiddleName);
            this.Controls.Add(this.lblEmployeeFirstName);
            this.Controls.Add(this.lblEmployeeID);
            this.Name = "Employees";
            this.Text = "Служители";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEmployeeID;
        private System.Windows.Forms.Label lblEmployeeFirstName;
        private System.Windows.Forms.Label lblEmployeeMiddleName;
        private System.Windows.Forms.Label lblEmployeeLastName;
        private System.Windows.Forms.Label lblEployeePosition;
        private System.Windows.Forms.TextBox txtEmployeeID;
        private System.Windows.Forms.TextBox txtEmployeeFirstName;
        private System.Windows.Forms.TextBox txtEmployeeMiddleName;
        private System.Windows.Forms.TextBox txtEmployeeLastName;
        private System.Windows.Forms.TextBox txtEmployeePosition;
        private System.Windows.Forms.Button btnEmployeeAdd;
        private System.Windows.Forms.Button btnEmployeeEdit;
        private System.Windows.Forms.Button btnEmployeeDelete;
        private System.Windows.Forms.TextBox txtEmployeeEGN;
        private System.Windows.Forms.Label lblEmployeeEGN;
    }
}